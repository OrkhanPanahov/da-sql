select * from sales_data;
-- 1 - Veril?n m?nb?y? g�r? ilk h?rfi C il? ba?layan m�?t?ri adlar?n?n qeyd olunmas?
select
        customer_name
from
        sales_data
where
        customer_name like 'C%';
        
--2 - Veril?n m?nb?y? g�r? m�?t?ri �nvan? Suite il? ba?layan m�?t?ri ad? v? �nvanlar?n?n qeyd olunmas?       
select
        customer_name,customer_address
from
        sales_data
where
        CUSTOMER_ADDRESS like 'Suite%';
        
 --3 - Veril?n m?nb?y? g�r? vahid qiym?ti 728-? v? �mumi d?y?ri 3000-? b?rab?r olmayan m?hsul adlar?n?n qeyd olunmas?       
select
        product_name
from
        sales_data
where
        unit_price<>728 and total_price!=3000;
        
--4 - Veril?n m?nb?y? g�r? m�?t?ri �lk?si il? m�?t?ri  ??h?rinin  birl??diril?r?k qeyd olunmas?
select
        concat(customer_country,customer_city)
from
        sales_data;
        
--5 - Veril?n m?nb?y? g�r? �mumi d?y?ri 7000-d?n b�y�k olan m?hsul id-l?rinin azalan s?ra il?, m�?t?ri �lk?l?ri v? �mumi d?y?rl?rin qeyd olunmas?      
select
        product_id,customer_country,total_price
from
        sales_data
where
        total_price>7000
order by
        to_number(product_id) desc;
    
--6 - Veril?n m?nb?y? �zr? 2000-ci il v? 3-c� aya v? ya 2001-ci il 2-ci aya g�r? abun? planlar?n?n  adlar?n?n qeyd olunmas?
select
        SUBSCRIPTION_PLAN, year_, month_
from
        sales_data
where
        (year_=2000 and month_=3) or ( year_=2001 and month_=2);

--7 - Veril?n m?nb? �zr? Asia v? Avropaya g�r? m�?t?ri �lk?l?rinin qeyd olunmas?
select
        customer_country, sales_rep_region
from
        sales_data
where
        sales_rep_region='Asia' or sales_rep_region='Europe' ;

--8 - Veril?n m?nb?y? g�r? 2020 v? 2023-c� ill?r �zr? h?ft?l?rin qeyd olunmas?
select
       week, year_
from
        sales_data
where
        year_ in (2020,2023);

--9 - Veril?n m?nb?y? g�r? 2020 v? 2023-c� ill?r �zr? h?ft?l?rin v? ill?rin artan s?ra il? qeyd olunmas?
select
       week, year_
from
        sales_data
where
        year_ in (2020,2023)
order by to_number(week) asc, year_;

--10 - Veril?n m?nb?y? g�r? m?hsul adlar?na g�r? m?hsul brendi,�?kisi, h�nd�rl�y�, eni v? d?rinliyinin birl??diril?r?k qeyd olunmas?
select
        product_name,product_brand || PRODUCT_WEIGHT || product_height || product_width || product_depth
from
        sales_data;
