select * from sales_data;
--1
select
        customer_name
from
        sales_data;
--2       
select
        customer_name
from
        sales_data
where
        CUSTOMER_ADDRESS = '3rd Floor';
 --3       
select
        product_name,unit_price
from
        sales_data
where
        unit_price>=728;
--4
select
        product_id,customer_country,total_price
from
        sales_data
where
        total_price>7000;
--5      
select
        product_id,customer_country,total_price
from
        sales_data
where
        total_price>7000
order by
        product_id asc;
    
--6
select
        SUBSCRIPTION_PLAN
from
        sales_data
where
        year_=2000 and month_=3;

--7
select
        customer_country
from
        sales_data
where
        sales_rep_region='Asia' and device_type='Mobile' and age<30;

--8
select
        time_,week,month_,quarter,year_
from
        sales_data
where
        year_=2020;

--9
select
        product_name,shipping_cost,shipping_cost*0.5,total_price
from
        sales_data
where
        total_price>5000
order by
        total_price desc;

--10
select
        product_name,quantity,product_brand,product_code, PRODUCT_WEIGHt, product_height, product_width, product_depth
from
        sales_data
where
        product_brand= 'Adidas' and PRODUCT_WEIGHt<=50 and product_height<=50 and product_width<=50 and product_depth<=50;
